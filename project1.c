// Project: Caeser Cipher Breaker (using Least Square)
// Author:  Foxes

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <stdio.h>
#include <ctype.h>


using namespace std;

/**
    Fills array given[] with English letter frequencies
    @param given the array to be filled
    @param fname the input file containing the frequencies
*/
void readFreq(float given[], char fname[]) 
{

    int x = 0;
    string line;
    fstream myfile(fname);

    if (myfile.is_open()) {
        while (myfile.good()) {
            getline(myfile, line);
            char * pointer;
            pointer = & (line[1]);
            given[x] = atof(pointer);
            x++;
        }
        myfile.close();
    }

}

/**
    Calculates frequencies of encrypted input, adds to array found[]
    @param found[] array to hold frequencies
    @param fname the input file containing the encrypted string
*/
void calcFreq(float found[], char fname[]) 
{

    int i = 0;
    int y = 0;
    int len;

    string line;
    fstream myfile(fname);

    if (myfile.is_open()) {

        while (myfile.good()) {

            getline(myfile, line);

        }

        myfile.close();
    }

    for (i = 0; i < len; i++) {

        found[tolower(line[i]) - 'a']++;

    }

    for (y = 0; y < 26; y++) {

        found[y] = found[y] / len;

    }

}

/**
    Rotates entire array by 1
    @param num, size of the array
    @param given[], array to be rotated
*/
void rotateByOne(float given[], int num)
{

  int i;
  float temp;
  temp = given[0];
  for (i = 0; i < num-1; i++)
     given[i] = given[i+1];
  given[i] = temp;
}
 

/**
    Finds key needed for decryption
    @param given[], containing average frequencys of English leters
    @param found[], containing frequencies found from encrypted string
*/
 int findKey(float given[], float found[]) 
 {

    float totals[26] = {0}; //array to hold the frequencies at each rotation
    float result;
    float square;
    float total = 0;
    int i = 0;
    int x = 0;
    int c = 0;
    int mIndex = 0;

    for (x = 0; x < 26; x++) {
        for (i = 0; i < 26; i++) {

            float result = 0;
            float square = 0;

            result = given[i] - found[i];
            square = result * result;

            total = total + square;
        }

        totals[x] = total;
        total = 0;

        rotateByOne(found, 25);

    }

    float min = FLT_MAX;

    //finding the minimal value in the array totals[]
    for (c = 1; c < 26; c++) {
        if (totals[c] < min) {
            min = totals[c];
            mIndex = c;
        }
    }

    return mIndex;

}   

/**
    Helper function for decrypt, shifts a single char by k
    @param ch, char to be shifted
    @param k, # of shifts to be made
*/
char decryptHelper(char ch, int k)
{
    if ( k < 0 )
        k = k + 26;

    if ( isupper(ch) )
        return (ch - 'A' + (26 - k)) % 26 + 'A';

    if ( islower(ch) )
        return (ch - 'a' + (26 - k)) % 26 + 'a';

    return ch;
}

/**
    Opens file, decrypts string, prints decrypted string to file
    @param key, the # of shifts to be made for decryption
    @param fname[], filename containing encrypted string
*/
void decrypt(int key, char fname[]) 
{

    char c[1000];
    FILE * fptr;
    if ((fptr = fopen(fname, "r")) == NULL) {
        printf("Error! opening file");
        exit(1); /* Program exits if file pointer returns NULL. */
    }
    fscanf(fptr, "%[^\n]", c);
    fclose(fptr);

    for (int i = 0; i < sizeof c - 1; ++i)
        c[i] = decryptHelper(c[i], key);

    FILE * fptz;
    fptz = fopen(fname, "w");
    if (fptz == NULL) {
        printf("Error!");
        exit(1);
    }
    fprintf(fptz, "%s", c);
    fclose(fptz);

}

/**
    Main method
*/
int main(int argc, char *argv[])
{
   

    int i = 0;
    float given[26];
    float found[26] = {0};

    readFreq(given,"LetFreq.txt");
    calcFreq(found,argv[1]);


    decrypt (findKey(given,found), "data.txt");


return 0;
}