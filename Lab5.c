#include <stdio.h>
#include <stdlib.h>
 
struct Mark {       
    int x;
    int y;
};
 
void getInfo (FILE * f, struct Mark * p);
void printInfo (FILE * f, struct Mark item);
int  compare(struct Mark *, struct Mark *);
typedef int (*compfn)(const void*, const void*);

 
int main(int argc, char* argv[])   {
    Mark list[74];
    //Mark mark;
    int size = 0, i, col = 0;

    struct Mark mark;

    FILE * fin, *fout;
 
    if (argc < 3)   {
        printf ("Usage: prog_y infile, outfile\n");
        exit(1);
    }
    fin = fopen (argv[1], "r");
    fout = fopen (argv[2], "w");
    if ( fin == NULL || fout == NULL )  {
        printf ("Cannot open file(s)\n");
        exit(1);
    }


    while ( ! feof(fin) ) {
        getInfo (fin, &mark);
        list[size++] = mark;
        //printInfo (fout, mark);
    }

    

    qsort((void *) &list,                             // Beginning address of array
                    74,                                 // Number of elements in array
                    sizeof(struct Mark),              // Size of each element
                    (compfn)compare );                  // Pointer to compare function


       for (i = 0; i < size; i++)
    {


        printInfo (stdout, list[i]);
        if ( ++col % 5 == 0 )
            printf("\n");
    }

    fclose (fin);
    fclose (fout);
 
    return 0;
}
 
void getInfo (FILE* f, struct Mark * p)  {
    fscanf (f, "%d%d", &(p->x), &(p->y));
}
 
void printInfo (FILE* f, struct Mark item)  {
    fprintf (f, "(%d,%d)", item.x, item.y);
    //(" " for _ in range(100)] for y in range(100)]
}
int compare(struct Mark *elem1, struct Mark *elem2)
{
   if ( elem1->x < elem2->x)
      return -1;

   else if (elem1->x > elem2->x)
      return 1;

   else if ( elem1->y < elem2->y)
      return -1;

   else if (elem1->y > elem2->y)
      return 1;
  else
    return 0;

     // return 0;
}