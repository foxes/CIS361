all: cipher project1 run

cipher: cipher.c
	g++ -o cipher cipher.c

project1: project.c
	g++ -o project1 project1.c

run: 
	./cipher 1 5 TestInput1.txt TestDecrypt1.txt
	./cipher 1 8 TestInput2.txt TestDecrypt2.txt
	./project1 TestDecrypt1.txt
	./project1 TestDecrypt2.txt
	diff -s TestInput1.txt TestDecrypt1.txt
	diff -s TestInput2.txt TestDecrypt2.txt
